package com.test.periodicdataprocessing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements DownloadResultReceiver.Receiver{
	DownloadResultReceiver mReceiver;
	
	// adding comment for testin
	String[] results=null;
	ListView periodicDateList;
	final String url = "http://javatechig.com/api/get_category_posts/?dev=1&slug=android";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);

		periodicDateList=(ListView)findViewById(R.id.periodicDateList);

		Button sendRequestToService=(Button)findViewById(R.id.downloadButton);
		sendRequestToService.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				/* Starting Download Service */
				mReceiver = new DownloadResultReceiver(new Handler());
				mReceiver.setReceiver(MainActivity.this);
				Intent intent = new Intent(Intent.ACTION_SYNC, null, MainActivity.this, DownloadService.class);

				/* Send optional extras to Download IntentService */
				intent.putExtra("url", url);
				intent.putExtra("receiver", mReceiver);
				intent.putExtra("requestId", 101);

				startService(intent);
			}
		});
	}
	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		
		switch (resultCode) {
		case DownloadService.STATUS_RUNNING:
			Log.d("SERVICE", "The service is running.");
			setProgressBarIndeterminateVisibility(true);
			break;
			
		case DownloadService.STATUS_FINISHED:
			/* Hide progress & extract result from bundle */
			setProgressBarIndeterminateVisibility(false);
 
			results = resultData.getStringArray("result");
		//System.out.println("Results from web service are");
		for(String value :results)
		{
			Log.d("RESULT", value);
		}
			/* Update ListView with result */
			ArrayAdapter arrayAdapter = new ArrayAdapter(MainActivity.this, R.layout.items, results);
			periodicDateList.setAdapter(arrayAdapter);
			break;

		case DownloadService.STATUS_ERROR:
			/* Handle the error */
			// bye commit
			String error = resultData.getString(Intent.EXTRA_TEXT);
			Toast.makeText(this, error, Toast.LENGTH_LONG).show();
			break;
		}
	}
}
